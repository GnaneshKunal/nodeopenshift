/*const express = require('express');
const app = express();
const http = require('http');

const server = http.createServer(app);
const PORT = process.env.OPENSHIFT_NODEJS_PORT || 8080;
const IP = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";

app.get('/', (req, res) => {
    res.status(200).send({
        status: true,
        message: 'Hello World'
    });
});

app.listen(PORT, IP, () => {
    console.log(`Server is running on ${IP}:${PORT}`);
});*/

var express = require('express');
var app = express();
var fs      = require('fs');
var parser  = require('body-parser');

//Setup ip adress and port
var ipaddress ;

function initIPAdress() {
    var adr = process.env.OPENSHIFT_NODEJS_IP;
    if (typeof adr === "undefined") {
            //  Log errors on OpenShift but continue w/ 127.0.0.1 - this
            //  allows us to run/test the app locally.
            console.warn('No OPENSHIFT_NODEJS_IP var, using localhost');
            adr = 'localhost';
    }

    ipaddress = adr;
}

var port      = process.env.OPENSHIFT_NODEJS_PORT || 8080;


app.get('/', function (req, res) {
  res.send('Hello World!')
})

app.get('/admin', function (req, res) {
        res.setHeader('Content-Type', 'text/html'); 
        res.send( fs.readFileSync('./index_admin.html') );
})

initIPAdress(); //Setup IP adress before app.listen()

app.listen(port, ipaddress, function() {
        console.log('%s: Node server started on %s:%d ...',
                        Date(Date.now() ), ipaddress, port);
});